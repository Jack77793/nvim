vim.o.guifont = "SarasaTermSC Nerd Font:h15"
vim.g.neovide_floating_shadow = true
vim.g.neovide_transparency = 0.9
vim.g.neovide_refresh_rate = 60
vim.g.neovide_refresh_rate_idle = 5
vim.g.neovide_no_idle = false
vim.g.neovide_remember_window_size = true
vim.g.neovide_cursor_vfx_mode = "pixiedust"

vim.cmd [[ TransparentDisable ]]
