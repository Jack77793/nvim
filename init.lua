require("keyb")
require("plug")
require("conf")

if vim.g.neovide then
    require("gui")
end
